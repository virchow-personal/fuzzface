package how.virc.fuzzface;

/**
 * Created by afshar on 10/4/17.
 */

public class FuzzZekr {

    private int mCount = 98;

    public void inc() {
        mCount++;
        if (mCount == 1000) {
            mCount = 0;
        }
    }

    public String get() {
        return mCount + "";
    }

    public void reset() {
        mCount = 0;
    }

}
