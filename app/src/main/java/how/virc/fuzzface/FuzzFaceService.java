/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package how.virc.fuzzface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowInsets;
import android.widget.Toast;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Digital watch face with seconds. In ambient mode, the seconds aren't displayed. On devices with
 * low-bit ambient mode, the text is drawn without anti-aliasing in ambient mode.
 */
public class FuzzFaceService extends CanvasWatchFaceService {


    /**
     * Update rate in milliseconds for interactive mode. We update once a second since seconds are
     * displayed in interactive mode.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);

    /**
     * Handler message id for updating the time periodically in interactive mode.
     */
    private static final int MSG_UPDATE_TIME = 0;

    private FuzzTime mTime;
    private FuzzZekr mZekr;

    private static Context sAppContext;

    public static Context getAppContext() {
        return sAppContext;
    }

    @Override
    public Engine onCreateEngine() {
        return new Engine();
    }


    public class Engine extends CanvasWatchFaceService.Engine {

        final Handler mUpdateTimeHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        Engine.this.handleUpdateTimeMessage();
                        break;
                }
            }

        };

        boolean mRegisteredTimeZoneReceiver = false;
        Paint mBackgroundPaint;
        TextPaint mTextPaint;
        TextPaint mTextMiniPaint;
        Typeface mTypeface;
        boolean mAmbient;
        //Calendar mCalendar;
        FuzzTime mTime;
        final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };
        float mXOffset;
        float mYOffset;


        /**
         * Whether the display supports fewer bits for each color in ambient mode. When true, we
         * disable anti-aliasing in ambient mode.
         */
        boolean mLowBitAmbient;

        FuzzLayouter mLayout;

        @Override
        public void onNotificationCountChanged(int count) {
            Log.d("notification", "changed: " + count);
            mLayout.mSlotSub1.setText(count + "");
        }

        @Override
        public void onCreate(SurfaceHolder h) {
            super.onCreate(h);
            sAppContext = getApplicationContext();



            Log.d("create", "onCreate");

            setWatchFaceStyle(new WatchFaceStyle.Builder(FuzzFaceService.this)

                    .setAcceptsTapEvents(true)
                    .build());
            Resources resources = FuzzFaceService.this.getResources();
            mYOffset = resources.getDimension(R.dimen.digital_y_offset);

            mBackgroundPaint = new Paint();
            mBackgroundPaint.setColor(resources.getColor(R.color.background));

            mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            mTextPaint.setColor(resources.getColor(R.color.digital_text));

            mTextMiniPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            mTextMiniPaint.setColor(resources.getColor(R.color.digital_text));

            mTime = new FuzzTime();
            mZekr = new FuzzZekr();

            mTypeface = Typeface.createFromAsset(getAssets(), "fonts/font.ttf");

        }

        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }


        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();

                // Update time zone in case it changed while we weren't visible.
                //mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            } else {
                unregisterReceiver();
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            FuzzFaceService.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            FuzzFaceService.this.unregisterReceiver(mTimeZoneReceiver);
        }

        @Override
        public void onApplyWindowInsets(WindowInsets insets) {
            super.onApplyWindowInsets(insets);

            // Load resources that have alternate values for round watches.
            Resources resources = FuzzFaceService.this.getResources();
            boolean isRound = insets.isRound();
            mXOffset = resources.getDimension(isRound
                    ? R.dimen.digital_x_offset_round : R.dimen.digital_x_offset);
            float textSize = resources.getDimension(isRound
                    ? R.dimen.digital_text_size_round : R.dimen.digital_text_size);

            mTextPaint.setTextSize(textSize);
            mTextMiniPaint.setTextSize(resources.getDimension(R.dimen.digital_text_size_small));
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            if (mAmbient != inAmbientMode) {
                mAmbient = inAmbientMode;
                if (mLowBitAmbient) {
                    mTextPaint.setAntiAlias(!inAmbientMode);
                }
                invalidate();
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }


        private long lastTap = System.currentTimeMillis();
        /**
         * Captures tap event (and tap type) and toggles the background color if the user finishes
         * a tap.
         */
        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {

            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    // The user has started touching the screen.
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                    // The user has started a different gesture or otherwise cancelled the tap.
                    break;
                case TAP_TYPE_TAP:
                    // The user has completed the tap gesture.
                    // TODO: Add code to handle the tap gesture.
                    long now = System.currentTimeMillis();
                    Log.d("logged", (now - lastTap) + "");

                    long diff = now - lastTap;
                    if (diff < 200) {
                        mZekr.reset();
                    } else {
                        mZekr.inc();
                    }
                    mLayout.mSlotSub3.setText(mZekr.get());
                    lastTap = now;
                    break;
            }
            invalidate();
        }


        private float mCenterX;
        private float mCenterY;

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            mLayout = new FuzzLayouter(width, height);
            mLayout.mSlotSub2.setText(getString(R.string.message));
            Log.d("surface", "onsurfacechanged");
        }


        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            // Draw the background.
            if (isInAmbientMode()) {
                canvas.drawColor(Color.BLACK);
            } else {
                canvas.drawRect(0, 0, bounds.width(), bounds.height(), mBackgroundPaint);
            }

            mLayout.mSlotMain.setText(mTime.getTime());
            mLayout.mSlotSub0.setText(mTime.getDate());


            // Draw H:MM in ambient mode or H:MM:SS in interactive mode.
            //long now = System.currentTimeMillis();
            //mCalendar.setTimeInMillis(now);


            float textWidth = canvas.getWidth() - (mXOffset * 2);
            String text = mTime.getTime();


            StaticLayout textLayout = new StaticLayout(
                    text, mTextPaint, (int) textWidth, Layout.Alignment.ALIGN_CENTER, 0.9f, 0.0f, true);
            //canvas.drawText(text, mXOffset, mYOffset, mTextPaint);
            mTextPaint.setTypeface(mTypeface);

            mTextMiniPaint.setTypeface(mTypeface);


            StaticLayout miniLayout = new StaticLayout(
                    "th", mTextMiniPaint, (int) (canvas.getWidth() / 3), Layout.Alignment.ALIGN_NORMAL, 0.9f, 0.0f, false
            );

            mTextPaint.setLetterSpacing(-0.10f);

            canvas.save();
            canvas.translate(mXOffset, mYOffset * 3);
            //textLayout.draw(canvas);
            canvas.restore();

            canvas.save();
            canvas.translate(mXOffset, canvas.getHeight() - miniLayout.getHeight() - mYOffset);


//            makeTextLayout(getString(R.string.message),
//                    getResources().getDimension(R.dimen.digital_text_size_small),
//                    (int) (canvas.getWidth() / 3),
//                    getResources().getColor(R.color.digital_text),
//                    Layout.Alignment.ALIGN_NORMAL).draw(canvas);

            //miniLayout.draw(canvas);
            canvas.restore();

            mLayout.draw(canvas);
        }


        private StaticLayout makeTextLayout(String text, float textSize, int textWidth, int textColor, Layout.Alignment textAlign) {
            TextPaint p = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            p.setColor(textColor);
            p.setLetterSpacing(-0.10f);
            p.setTypeface(mTypeface);
            p.setTextSize(textSize);
            StaticLayout l = new StaticLayout(text, p, textWidth, textAlign, 0.9f, 0.0f, false);
            return l;
        }

        /**
         * Starts the {@link #mUpdateTimeHandler} timer if it should be running and isn't currently
         * or stops it if it shouldn't be running but currently is.
         */
        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer should
         * only run when we're visible and in interactive mode.
         */
        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        void handleUpdateTimeMessage() {
            invalidate();

            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS
                        - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
            }
        }
    }
}
