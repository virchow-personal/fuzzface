package how.virc.fuzzface;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.support.wearable.complications.ComplicationData;
import android.support.wearable.complications.ComplicationText;
import android.support.wearable.complications.rendering.ComplicationDrawable;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;

/**
 * Created by afshar on 10/4/17.
 */

public class FuzzLayouter {

    Typeface mDefaultTypeface;
    SparseArray<Slot> mSlots = new SparseArray<>();
    Slot mSlotMain;
    Slot mSlotSub0;
    Slot mSlotSub1;
    Slot mSlotSub2;
    Slot mSlotSub3;
    int mWidth;
    int mHeight;
    int mPad;



    public FuzzLayouter(int width, int height) {
        mWidth = width;
        mHeight = height;
        Context c = FuzzFaceService.getAppContext();
        mDefaultTypeface = Typeface.createFromAsset(c.getAssets(), "fonts/font.ttf");
        mPad = (int) c.getResources().getDimension(R.dimen.digital_x_offset);


        mSlotMain = add("main");
        mSlotMain.mSize = c.getResources().getDimension(R.dimen.digital_text_size);
        mSlotMain.mWidth = (int) mWidth - mPad * 2;
        mSlotMain.mX = mPad;
        mSlotMain.mY = mPad * 2.5f;


        float bottom = mHeight - mPad;
        float middle = mWidth / 2;



        mSlotSub0 = add("0");
        mSlotSub0.mX = mSlotMain.mX;
        mSlotSub0.mSize = c.getResources().getDimension(R.dimen.digital_text_size_small);
        mSlotSub0.mY = bottom - mSlotSub0.mSize * 3.5f;
        mSlotSub0.mWidth = mSlotMain.mWidth;
        mSlotSub0.mAlign = Layout.Alignment.ALIGN_CENTER;


        mSlotSub1 = add("");
        mSlotSub1.mSize = c.getResources().getDimension(R.dimen.digital_text_size_small) * 1.5f;
        mSlotSub1.mY = bottom - mSlotSub1.mSize;
        mSlotSub1.mX = mSlotMain.mX;
        mSlotSub1.mWidth = mSlotMain.mWidth;
        mSlotSub1.mAlign = Layout.Alignment.ALIGN_NORMAL;
        mSlotSub1.mBackColor = c.getColor(R.color.notifications_text);
        mSlotSub1.mColor = c.getColor(R.color.background);


        mSlotSub2 = add("2");
        mSlotSub2.mSize = c.getResources().getDimension(R.dimen.digital_text_size_small);
        mSlotSub2.mY = bottom - mSlotSub2.mSize - mPad;
        mSlotSub2.mX = mSlotMain.mX;
        mSlotSub2.mWidth = mSlotMain.mWidth;
        mSlotSub2.mAlign = Layout.Alignment.ALIGN_CENTER;
        mSlotSub2.mTypeface = Typeface.SERIF;
        mSlotSub2.mColor = c.getColor(R.color.holy_text);


        mSlotSub3 = add("0");
        mSlotSub3.mSize = c.getResources().getDimension(R.dimen.digital_text_size_small) * 1.5f;
        mSlotSub3.mY = bottom - mSlotSub3.mSize;
        mSlotSub3.mX = mWidth - mSlotSub3.mSize - mPad;
        mSlotSub3.mWidth = mSlotMain.mWidth;
        mSlotSub3.mAlign = Layout.Alignment.ALIGN_CENTER;
        mSlotSub3.mColor = c.getColor(R.color.background);
        mSlotSub3.mBackColor = c.getColor(R.color.light_grey);


    }

    public void draw(Canvas c) {
        mSlotMain.draw(c);
        mSlotSub0.mY = mSlotMain.mLayout.getHeight() + mSlotMain.mY;
        mSlotSub0.makeTextLayout();
        mSlotSub0.draw(c);
        mSlotSub2.draw(c);

        if (!mSlotSub1.mText.equals("0") && mSlotSub1.mText.length() > 0) {
            mSlotSub1.drawComplication(c);

        }

        if (!mSlotSub3.mText.equals("0") && mSlotSub3.mText.length() > 0) {
            mSlotSub3.drawComplication(c);
        }


    }


    public Slot add(String text) {
        Slot s = new Slot();
        s.mColor = FuzzFaceService.getAppContext().getColor(R.color.light_grey);
        s.mTypeface = mDefaultTypeface;
        s.setText(text);
        return s;
    }

    public class Slot {
        boolean mVisible = true;
        int mColor;
        int mBackColor;
        float mSize;
        Typeface mTypeface;
        Layout.Alignment mAlign;
        int mWidth;
        float mX;
        float mY;
        String mText;
        TextPaint mPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        StaticLayout mLayout;

        public void setText(String t) {
            mText = t;
            mLayout = makeTextLayout();
        }

        public void draw(Canvas c) {
            c.save();
            c.translate(mX, mY);
            mLayout.draw(c);
            c.restore();
        }

        private StaticLayout makeTextLayout() {
            mPaint.setColor(mColor);
            mPaint.setLetterSpacing(-0.10f);
            mPaint.setTypeface(mTypeface);
            mPaint.setTextSize(mSize);
            return new StaticLayout(mText, mPaint, mWidth, mAlign, 0.75f, 0.0f, true);
        }

        public void drawComplication(Canvas c) {
            ComplicationDrawable dr = new ComplicationDrawable(FuzzFaceService.getAppContext());
            dr.setTextColorActive(mColor);
            dr.setBackgroundColorActive(mBackColor);
            //dr.setBorderColorActive(mColor);
            //dr.setBorderWidthActive(4);
            dr.setIconColorActive(Color.BLUE);

            Context ctx = FuzzFaceService.getAppContext();
            ComplicationText t = ComplicationText.plainText(mText);
            ComplicationData d = new ComplicationData.Builder(ComplicationData.TYPE_SHORT_TEXT)
                    .setShortText(t)
                    //.setShortTitle(t)
                    //.setLongText(t)
                    //.setIcon(Icon.createWithResource(FuzzFaceService.getAppContext(), R.drawable.add_complication))
                    //.setIcon(Icon.createWithResource(FuzzFaceService.getAppContext(), R.drawable.add_complication))
                    .build();
            //dr.setBounds((int) 20, (int) 20, (int) 80, (int) 80);
            dr.setBounds((int) mX, (int) mY, (int) (mX + mSize), (int) (mY + mSize));
            dr.setComplicationData(d);
            //Drawable dw = ctx.getResources().getDrawable(R.drawable.ic_cc_checkmark);


            //float dws = mSize / 4;
            //dw.setBounds((int) (mX + mSize * 0.375), (int) (mY + mSize * 0.125), (int) (mX + mSize * 0.625), (int) (mY + mSize * 0.375));
            //dw.draw(c);
            dr.draw(c, System.currentTimeMillis());
        }
    }

}
