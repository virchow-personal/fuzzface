package how.virc.fuzzface;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Fuzzy clock.
 */
public class FuzzTime {

    private String[] hours;
    private String[] times;
    private String[] seasons;

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    private String dateFormat;

    private static int[] sMinutes = new int[]{
            0,  0,  0,  1,  1,  1,  1,  1,  2,  2,  // 0-9
            2,  2,  2,  3,  3,  3,  3,  3,  4,  4,  // 10-19
            4,  4,  4,  5,  5,  5,  5,  5,  6,  6,  // 20-29
            6,  6,  6,  7,  7,  7,  7,  7,  8,  8,  // 30-39
            8,  8,  8,  9,  9,  9,  9,  9,  10, 10, // 40-49
            10, 10, 10, 11, 11, 11, 11, 11, 12, 12  // 50-59
    };

    public FuzzTime() {
        this(FuzzDefaults.defaultTimes,
                FuzzDefaults.defaultHours,
                FuzzDefaults.defaultDateFormat);
    }

    public FuzzTime(String[] times, String[] hours, String dateFormat) {
        this.times = times;
        this.hours = hours;
        this.dateFormat = dateFormat;
    }

    /**
     * Creates the given hour of the day as a word.
     * @param hourOfDay hour of day 1-12
     * @return word representation of the hour.
     */
    private String getHour(int hourOfDay) {
        // 11 here is +12 -1 for safe positive modulo and 1-indexed hours.
        return hours[Math.abs((hourOfDay + 11) % 12)];
    }

    private String getSeason(int month) {
        return seasons[month - 1];
    }

    /**
     * Creates a fuzzy representation of a given time.
     * @param calendar the time to represent.
     * @return said fuzzy representation.
     */
    public String getTime(Calendar calendar) {
        return String.format(
                times[sMinutes[calendar.get(Calendar.MINUTE)]],
                getHour(calendar.get(Calendar.HOUR_OF_DAY)),
                getHour(calendar.get(Calendar.HOUR_OF_DAY) + 1)
        );
    }

    public String getDate(Calendar calendar) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(this.dateFormat);
        return dateFormat.format(calendar.getTime()).toLowerCase();
    }

    public String getDate() {
        return getDate(Calendar.getInstance(TimeZone.getDefault()));
    }

    /**
     * Creates a fuzzy representation of the current time.
     * @return said fuzzy representation.
     */
    public String getTime() {
        return getTime(Calendar.getInstance(TimeZone.getDefault()));
    }


    public String getSeason() {
        return getSeason(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.MONTH));
    }

}
