
package how.virc.fuzzface;

import android.app.Activity;
import android.os.Bundle;


public class FuzzConfig extends Activity {

    private static final String TAG = "ConfigActivity";

    static final int COMPLICATION_CONFIG_REQUEST_CODE = 1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_config);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // TODO: Step 3, release
        // Required to release retriever for active complication data.
    }


}