package how.virc.fuzzface;

/**
 * Created by afshar on 10/2/17.
 */



/**
 * Created by afshar on 7/31/14.
 */
public class FuzzDefaults {
    public static final String[] defaultHours = new String[]{
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve"
    };

    public static final String[] defaultTimes = new String[]{
            "%1$s o'clock",             // 0
            "five past %1$s",           // 1
            "ten past %1$s",            // 2
            "quarter past %1$s",        // 3
            "twenty past %1$s",         // 4
            "twenty-five past %1$s",    // 5
            "half past %1$s",           // 6
            "twenty-five to %2$s",      // 7
            "twenty to %2$s",           // 8
            "quarter to %2$s",          // 9
            "ten to %2$s",              // 10
            "five to %2$s",             // 11
            "%2$s o'clock",             // 12
    };

    public static final String defaultDateFormat = "EEE, MMM d";

}
